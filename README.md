# Gjav Patches

Custom patches for Gjav Drupal Website

## Module: Commerce

### Negate commerce product conditions

*negate_product_contitions.php*

Fintanto che non esiste la possibilità di negare le condizioni delle promozioni
o comunque escludere prodotti o categorie, ho modificato a mano le condizioni
invertendo TRUE e FALSE :-)

```php
OrderProductCategory.php
OrderItemProductCategory.php
```
Si trovano in: `/commerce/modules/product/src/Plugin/Commerce/Condition`

La funzione modificata è la `evaluate()` Quando trova corrispondenza, invece di
restituire TRUE restituisce FALSE, e se non trova nulla allora TRUE al posto di
FALSE.


### Custom minimum order total limit

Modificare la funzione:
  `public function formPage(RouteMatchInterface $route_match) {`
Che si trova nel file:
  commerce/modules/checkout/src/Controller/CheckoutController.php
Aggiungendo il seguente codice:
```php
    // Quando entro nel checkout o nella review, se il subtotale è minore di 29 euro blocco tutto
    if ($requested_step_id == 'order_information' || $requested_step_id == 'review') {
      $subtotal = $order->getSubtotalPrice();
      $subtotalPrice = $subtotal->getNumber();


      if (floatval($subtotalPrice) < 29 ){
        drupal_set_message("Il quantitativo minimo per un ordine è di 29,00€", 'warning');

        // Mando su una pagina custom, in questo caso la pagina 31 lo shop:
        $options = ['absolute' => TRUE];
        $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => 31], $options)->toString();

        // Mando alla home:
        // $url = \Drupal\Core\Url::fromRoute('<front>')->toString();

        return new RedirectResponse($url);
      }

    }
```


### Commerce COD Fee (Contrassegno)

Nel file PaymentInformation avevo aggiunto sia contrassegno che ricalcolo spedizioni
ma poi le spedizioni contano il fee sul totale e quindi sbagliano.

Il codice era:
```php
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_price\Price;
```

e sul *submitPaneForm()*:
```php

    if($payment_gateway->id() == 'cod'){

      // tolgo tutti gli adjustment di tipo fee con label "contributo per il contrassegno";
      $currentAdjustments = $this->order->collectAdjustments();
      foreach ($currentAdjustments as $adj) {
        if($adj->getType() == "fee" && $adj->getLabel() == "Contributo per il contrassegno"){
          $this->order->removeAdjustment($adj);
        }

        // Poi mi tocca anche togliere gli shipping perchè possono fare casino
        if($adj->getType() == "shipping"){
          $this->order->removeAdjustment($adj);
        }

      }

      $this->order->save();

      $adjustments[] = new Adjustment([
        'type' => 'fee',
        'label' => 'Contributo per il contrassegno',
        'amount' => new Price('4.00', 'EUR'),
        'locked' => true
      ]);
      $this->order->addAdjustment($adjustments[0]);

      // invece di mettere lo shipping con 59 euro, lo testo su 63 euro
      if($this->order->getTotalPrice()->getNumber() < 63){
        $adjustments[] = new Adjustment([
          'type' => 'shipping',
          'label' => 'Spedizione',
          'amount' => new Price('6.00', 'EUR'),
          'source_id' => 25,
          'locked' => true
        ]);
        $this->order->addAdjustment($adjustments[1]);
      }

      $this->order->save();
    } else{
      // tolgo tutti gli adjustment di tipo fee con label "contributo per il contrassegno";
      $currentAdjustments = $this->order->collectAdjustments();
      foreach ($currentAdjustments as $adj) {
        if($adj->getType() == "fee" && $adj->getLabel() == "Contributo per il contrassegno"){
          $this->order->removeAdjustment($adj);
        }
      }

    }
```

Quindi ho tenuto solo:
```php
    // -------------------------------------------------------------------------
    // Tolgo tutti gli adjustment di tipo fee con label
    // "contributo per il contrassegno";
    // -------------------------------------------------------------------------
    $currentAdjustments = $this->order->collectAdjustments();
    foreach ($currentAdjustments as $adj) {
      if($adj->getType() == "fee" && $adj->getLabel() == "Contributo per il contrassegno"){
        $this->order->removeAdjustment($adj);
      }
    }
    // Poi, solo se ho il contrassegno, allora riaggiungo il fee.
    // In questo modo poi quando si processa lo shipping riesco a fare i conti
    // giusti.
    if($payment_gateway->id() == 'cod'){
      $adjustments[] = new Adjustment([
        'type' => 'fee',
        'label' => 'Contributo per il contrassegno',
        'amount' => new Price('4.00', 'EUR'),
        'locked' => true
      ]);
      $this->order->addAdjustment($adjustments[0]);
    }
    // Salvo l'ordine.
    $this->order->save();
    // -------------------------------------------------------------------------
```

### Hide Commerce Review edit links

Normalmente nel pannello di revisione ordine i vari box hanno un link di
modifica che però crea confusione. Quindi tramite questa patch lo rimuovo.


### Display product code on add variation form

Di default l'elenco delle variations mostra la label. Per qualche motivo su
Gjav non riesco a modificare la label delle variations quindi per aggirare
il problema ho creato una patch "show_variation_code_on_dropdown.patch" che
aggiunge l'sku per differenziarli.


## Module: Commerce Shipping

### Contrassegno

Mentre va personalizzato il commerce shipping per non conteggiare il fee.
File: ShipmentOrderProcessor.php
Ho aggiunto in alto:
```php
use Drupal\commerce_price\Price;
use Drupal\commerce_payment\PaymentOption;
```

Va modificato il metodo *process()* nel seguente modo:
1. controllo se ho `$payment_gateway->id() == 'cod'`
2. allora tolgo tutti gli shipment
3. poi prendo il totale ordine
4. se il totale ordine è maggiore di 63 metto il free shipping
5. altrimenti metto le spedizioni a 6 euro.

Il codice va aggiunto alla fine di *process()*

```php
    // -------------------------------------------------------------------------
    // WARNING: QUESTO CODICE FUNZIONA SOLO PERCHÉ SU GJAV ABBIAMO SPEDIZIONI
    // SINGOLE
    // -------------------------------------------------------------------------
    // Tolgo tutti gli adjustment di tipo shipping se sono in "contrassegno" e
    // poi ricalcolo le spedizioni conteggiando il contrassegno. Questo codice
    // serve perché le spese di spedizione sono sul totale, e nel totale c'è
    // anche il contrassegno, ma questo no conta per il raggiungimento della
    // soglia.
    // Fintanto che su Commerce Shipping non ci sarà la possibilità di avere
    // condizioni sugli adjustments è necessario questo hack.
    // Inoltre ci sono alcune casistiche che non funzionano correttamente, qundi
    // quando non sono in COD, comunque tolgo le spedizioni e le ricalcolo.
    // In pratica le spedizioni le sovrascrivo sempre da qui indipendentemente
    // dalle condizioni impostate nella configurazione.
    // -------------------------------------------------------------------------
    // Carico il tipo di pagamento
    $payment_gateway = $order->get('payment_gateway')->entity;
    // Se sono in contrassegno (cod) allora altero il normale workflow
    if ($payment_gateway->id() == 'cod'){
      // Per prima cosa tolgo le spese di spedizione
      $currentAdjustments = $order->collectAdjustments();
      foreach ($currentAdjustments as $adj) {
        // Poi mi tocca anche togliere gli shipping perchè possono fare casino
        if($adj->getType() == "shipping"){
          $order->removeAdjustment($adj);
        }
      }
      // Poi verifico il totale ordine (che contiene 4 euro in più dati dal
      // contrassegno), quindi le spese di spedizione saranno gratis a partire
      // da 63 euro.
      // Il source ID non so come prenderlo, quindi riutilizzo quello che era
      // generato sopra.
      if ($order->getTotalPrice()->getNumber() < 63){
        // Carico le spese di spedizione normali
        $order->addAdjustment(new Adjustment([
          'type' => 'shipping',
          'label' => t('Shipping'),
          'amount' => new Price('6.00', 'EUR'),
          'source_id' => $shipment->id(),
        ]));
      } else {
        // Carico le spese di spedizione gratis
        $order->addAdjustment(new Adjustment([
          'type' => 'shipping',
          'label' => t('Shipping'),
          'amount' => new Price('0.00', 'EUR'),
          'source_id' => $shipment->id(),
        ]));
      }
    } else {
      // Per prima cosa tolgo le spese di spedizione
      $currentAdjustments = $order->collectAdjustments();
      foreach ($currentAdjustments as $adj) {
        // Poi mi tocca anche togliere gli shipping perchè possono fare casino
        if($adj->getType() == "shipping"){
          $order->removeAdjustment($adj);
        }
      }
      // Poi verifico il totale ordine (che contiene 4 euro in più dati dal
      // contrassegno), quindi le spese di spedizione saranno gratis a partire
      // da 63 euro.
      // Il source ID non so come prenderlo, quindi riutilizzo quello che era
      // generato sopra.
      if ($order->getTotalPrice()->getNumber() < 59){
        // Carico le spese di spedizione normali
        $order->addAdjustment(new Adjustment([
          'type' => 'shipping',
          'label' => t('Shipping'),
          'amount' => new Price('6.00', 'EUR'),
          'source_id' => $shipment->id(),
        ]));
      } else {
        // Carico le spese di spedizione gratis
        $order->addAdjustment(new Adjustment([
          'type' => 'shipping',
          'label' => t('Shipping'),
          'amount' => new Price('0.00', 'EUR'),
          'source_id' => $shipment->id(),
        ]));
      }
    }
    // -------------------------------------------------------------------------
```


## Module: simple_facebook_pixel
### Hide FB Pixel from amp
The patch `amp_facebook_pixel.patch` remove facebook pixel code from AMP pages in order to avoid AMP validation errors.
